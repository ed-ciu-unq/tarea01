module Tarea01Spec (spec) where
import Test.Hspec
import Tarea01 

spec :: Spec
spec =
  describe "Pruebas para la Tarea01" $ do
    it "sucesor" $
      sucesor (-1) `shouldBe` 0

    it "maximo. Caso 1" $
      maximo 3 8 `shouldBe` 8

    it "maximo. Caso 2" $
      maximo 8 3 `shouldBe` 8

    it "maximo. Caso 3" $
      maximo 8 8 `shouldBe` 8

    it "negar True" $
      negar True `shouldBe` False

    it "negar False" $
      negar False `shouldBe` True

    it "saludar conocidos" $
      saludar "Juan" `shouldBe` "Hola!"

    it "saludar conocidos" $
      saludar "Pablo" `shouldBe` "Hola!"

    it "saludar desconocido" $
      saludar "Pedro" `shouldBe` "Chau"
